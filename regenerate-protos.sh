#!/bin/bash

function clear_output_dir {
    if [ ! -d $1 ]
    then
    	mkdir -p $1
    else
    	rm -R $1/*
    fi
}

# Make sure that protoc is installed
if ! [ -x "$(command -v protoc)" ]; then
  echo 'Error: protoc is not installed.' >&2
  exit 1
fi

# Environment variables used during protoc calls
PROTOBUF_PATH=/usr/include
BASEPATH=lib/openfmb-ops-protobuf/proto/openfmb
SRC_PATHS="$BASEPATH/*.proto $BASEPATH/**/*.proto"

#########################################################
# C++
#########################################################

OUTPUTPATH=gen/cpp/openfmb/
clear_output_dir $OUTPUTPATH
if protoc --proto_path=$PROTOBUF_PATH --proto_path=$BASEPATH --cpp_out=$OUTPUTPATH $SRC_PATHS ;
then
  echo "Generated C++ protobuf files..."
else
  echo "C++ generation failed!!!"
  exit 2
fi
