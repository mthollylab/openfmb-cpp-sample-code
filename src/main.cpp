#include <iostream>

#include <nats/nats.h>
#include <string.h>

#include "uml.pb.h"
#include "commonmodule/commonmodule.pb.h"
#include "metermodule/metermodule.pb.h"

using namespace std;

const char          *topic = "openfmb.metermodule.MeterReadingProfile.>";
const char          *server = NULL;

static void onMsg(natsConnection *nc, natsSubscription *sub, natsMsg *msg, void *closure)
{
    printf("Received msg:   %s\n",
            natsMsg_GetSubject(msg));
    printf("Message length: %d\n",
        natsMsg_GetDataLength(msg));
    
    metermodule::MeterReadingProfile mrp;

    // Decode the protobuf message back into a structure we can interpret
    if (mrp.ParseFromArray(natsMsg_GetData(msg), natsMsg_GetDataLength(msg))) {
        // commonmodule::Timestamp* ts = mrp.mutable_readingmessageinfo()->mutable_messageinfo()->mutable_messagetimestamp();
        commonmodule::ReadingMessageInfo* rmi = mrp.mutable_readingmessageinfo();
        commonmodule::MessageInfo* mi = rmi->mutable_messageinfo();
        commonmodule::Timestamp* ts = mi->mutable_messagetimestamp();

        printf("  Message timestamp: %u.%u\n", 
            ts->seconds(),
            ts->fraction());
        
        commonmodule::Meter* meter = mrp.mutable_meter();
        commonmodule::ConductingEquipment* ce = meter->mutable_conductingequipment();
        __cxx11::string* mrid = ce->mutable_mrid();

        printf("  Meter MRID: %s\n",
            // mrp.mutable_meter()->mutable_conductingequipment()->mutable_mrid()->c_str());
            mrid->c_str());


        metermodule::MeterReading* mr = mrp.mutable_meterreading();
        commonmodule::ReadingMMXU* r_mmxu = mr->mutable_readingmmxu();
        commonmodule::WYE* watts = r_mmxu->mutable_w();
        commonmodule::CMV* net = watts->mutable_net();
        commonmodule::Vector* c_val = net->mutable_cval();
        commonmodule::AnalogueValue* mag = c_val->mutable_mag();
        google::protobuf::FloatValue* f = mag->mutable_f();

        printf("  W (net): %f\n",
            f->value());
    }


    natsMsg_Destroy(msg);
}

static void parseArgs(int argc, char **argv)
{
    for (int i = 1; (i < argc); i++)
    {
        if ((strcasecmp(argv[i], "-s") == 0) || (strcasecmp(argv[i], "--server") == 0)) {
            if (i + 1 == argc) {
                exit(100);
            }

            server = argv[++i];
        } else {
            printf("Unknown option: '%s'\n", argv[i]);
            exit(100);
        }
    }
    return;
}

int main(int argc, char **argv) {
    // Verify that the version of the library that we linked against is
    // compatible with the version of the headers we compiled against.
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    natsConnection      *conn = NULL;
    natsSubscription    *sub   = NULL;
    natsStatus          s;

    if (argc <= 1) {
        printf("Missing arguments...exiting...\n");
        exit(100);
    }
    parseArgs(argc, argv);

    s = natsConnection_ConnectTo(&conn, server);
    if (s != NATS_OK) {
        return 1;
    }
    printf("Connected to '%s'.\n", server);

    s = natsConnection_Subscribe(&sub, conn, topic, onMsg, NULL);
    if (s != NATS_OK) {
        return 2;
    }
    printf("Listening on '%s'.\n", topic);
    
    while (s == NATS_OK) {
        if (s == NATS_OK)
            nats_Sleep(1000);
    }

    // Destroy all our objects to avoid report of memory leak
    natsSubscription_Destroy(sub);
    natsConnection_Destroy(conn);
    nats_Close();

    return 0;
}
