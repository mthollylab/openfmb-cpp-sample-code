# OpenFMB C/C++ sample code

Sample C/C++ code for OpenFMB messages using NATS

## Setting up the development container when using Visual Studio Code

Follow these steps to open this sample in a container:

1. If this is your first time using a development container, please follow the [getting started steps](https://aka.ms/vscode-remote/containers/getting-started).

2. If you're not yet in a development container:
   - Clone this repository.
   - Press <kbd>F1</kbd> and select the **Remote-Containers: Open Folder in Container...** command.
   - Select the cloned copy of this folder, wait for the container to start, and try things out!

## Things to try

Once you have this sample opened in a container, you'll be able to work with it like you would locally.

> **Note:** This container runs as a non-root user with sudo access by default. Comment out `"-u", "vscode"` in `.devcontainer/devcontainer.json` if you'd prefer to run as root.
